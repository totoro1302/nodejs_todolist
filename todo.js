const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const app = express();

app.use(session({
	secret: 'keyboard',
	resave: false,
	saveUninitialized: true,
	cookie: {secure: false}
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use((req, res, next) => {
	if(typeof req.session.todolist === 'undefined'){
		req.session.todolist = [];
	}
	next();
});

app.get('/', (req, res) => {
	res.render('todo.ejs', {todolist: req.session.todolist});
});

app.get('/delete/:todo_id', (req, res) => {
	if(req.params.todo_id && req.session.todolist[req.params.todo_id] !== 'undefined'){
		req.session.todolist.splice(req.params.todo_id, 1);
	}

	res.redirect('/');
});

app.post('/save', (req, res) => {
	if(req.body.todo){
		req.session.todolist.push(req.body.todo);
	}
	res.redirect('/');
});

app.use((req, res, next) => {
	res.setHeader('Content-Type', 'text/plain');
	res.status(404).send('Page introuvable!!!');
});

app.listen(8080);